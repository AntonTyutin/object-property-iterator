<?php
/**
 * @package    minity/object-property-iterator
 * @copyright  2013 Anton Tyutin
 * @author     Anton Tyutin <anton@tyutin.ru>
 * @license    http://opensource.org/licenses/MIT   The MIT License
 */

namespace Minity;

use Iterator;
use OuterIterator;

/**
 * Iterating through collection of objects with getting key and value from object properties
 */
class ObjectPropertyIterator implements OuterIterator
{

	/**
	 * @var Iterator;
	 */
	private $inner;
	private $key;
	private $value;

	/**
	 * @param Iterator $iterator            Objects collection iterator
	 * @param string   $valuePropertyName   Property name for values of sequence
	 * @param string   $keyPropertyName     Property name for keys of sequence. If not set, 
	 *                                      the original iterator keys will be used.
	 */
	public function __construct(Iterator $iterator, $valuePropertyName, $keyPropertyName = null)
	{
		$this->inner = $iterator;
		$this->key = $keyPropertyName;
		$this->value = $valuePropertyName;
	}

	/**
	 * @return Iterator
	 */
	public function getInnerIterator()
	{
		return $this->inner;
	}

	public function current()
	{
		return $this->inner->valid() ? $this->getProperty($this->inner->current(), $this->value) : null;
	}

	public function key()
	{
		return $this->inner->valid()
			? (
				null !== $this->key
					? $this->getProperty($this->inner->current(), $this->key)
					: $this->inner->key()
			)
			: null;
	}

	public function next()
	{
		$this->inner->next();
	}

	public function rewind()
	{
		$this->inner->rewind();
	}

	public function valid()
	{
		return $this->inner->valid();
	}

	private function getProperty($object, $property)
	{
		return $object->{$property};
	}
}